import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import random
import os
import torch
from torch.utils.data import Dataset, DataLoader
from transformers import AutoModel, AutoConfig, AutoTokenizer, get_linear_schedule_with_warmup, TrainingArguments, Trainer
from datasets import load_metric
from transformers import BertModel
import torch.nn as nn
from torch.optim import Adam
from tqdm import tqdm


class Config:
    # data
    train_csv = '../data/train.csv'
    test_cssv = '../data/test.csv'
    sub_csv = '../data/sample_submission.csv'

    # model
    model = 'anferico/bert-for-patents'

    max_len = 32
    num_epoch = 2
    batch_size = 64
    epochs = 7
    lr = 1e-6

    train = False


train_df = pd.read_csv( Config.train_csv )
test_df = pd.read_csv( Config.test_cssv )
sub_df = pd.read_csv( Config.sub_csv )

score_map = dict(zip( range(5), ['0.00', '0.25', '0.50', '0.75', '1.00']))
inverse_score_map = dict(zip( [0.00, 0.25, 0.50, 0.75, 1.00],range(5) ))

train = pd.DataFrame()
train['text_input'] = train_df['anchor']+ '[sep]' + train_df['target'] + '[sep]' + train_df['context']
train['label'] = train_df['score'].map( inverse_score_map)